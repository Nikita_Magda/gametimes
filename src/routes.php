<?php
// Routes

use app\controllers\CommentsController;
use app\controllers\PostsController;
use app\controllers\UsersController;
use app\controllers\HomeController;

// UsersController actions
$app->get('/register', function ($request, $response, $args) {
    $usersController = new UsersController($this->renderer);

    return $usersController->registerPage($response);
});

$app->post('/register', function ($request, $response, $args) {
    $usersController = new UsersController($this->renderer);

    return $usersController->register($request, $response);
});

$app->get('/login', function ($request, $response, $args) {
    $usersController = new UsersController($this->renderer);

    return $usersController->loginPage($response);
});

$app->post('/login', function ($request, $response, $args) {
    $usersController = new UsersController($this->renderer);

    return $usersController->login($request, $response);
});

$app->any('/logout', function ($request, $response, $args) {
    $usersController = new UsersController($this->renderer);

    return $usersController->logout($response);
});

$app->any('/users/{id}', function ($request, $response, $args) {
    $usersController = new UsersController($this->renderer);

    return $usersController->get($request, $response);
});

// HomeController actions
$app->get('/', function ($request, $response, $args) {
    $homeController = new HomeController($this->renderer);

    return $homeController->indexPage($request, $response);
});

// PostsController actions
$app->get('/create-post', function ($request, $response, $args) {
    $postsController = new PostsController($this->renderer);

    return $postsController->createPage($request, $response);
});

$app->post('/create-post', function ($request, $response, $args) {
    $postsController = new PostsController($this->renderer);

    return $postsController->create($request, $response);
});

$app->get('/posts/{id}', function ($request, $response, $args) {
    $postsController = new PostsController($this->renderer);

    return $postsController->get($request, $response, $args);
});

$app->get('/search-posts', function ($request, $response, $args) {
    $postsController = new PostsController($this->renderer);

    return $postsController->search($request, $response);
});

// CommentsController actions
$app->post('/comments', function ($request, $response, $args) {
    $postsController = new CommentsController($this->renderer);

    return $postsController->addComment($request, $response);
});