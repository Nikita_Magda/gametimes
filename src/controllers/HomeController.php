<?php

namespace app\controllers;

use app\models\active_records\Category;
use app\models\active_records\Post;

class HomeController extends Controller
{
    private $renderer;

    public function __construct($renderer)
    {
        $this->renderer = $renderer;
    }

    public function indexPage($request, $response)
    {
        $params = $request->getQueryParams();

        $page = (isset($params['page']) && $params['page'] > 0) ? $params['page'] : 1;
        $limit = 10;

        $posts = Post::getNewest((($page - 1) * $limit), $limit);

        if (!isset($_SESSION['user'])) {
            $user = NULL;
        } else {
            $user = $_SESSION['user'];
        }

        $categories = Category::getAll();

        return $this->renderer->render($response, 'index.html', ['posts' => $posts, 'user' => $user, 'categories' => $categories]);
     }
}