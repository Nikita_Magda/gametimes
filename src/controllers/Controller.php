<?php
namespace app\controllers;

class Controller
{
    protected function redirect($response, $url)
    {
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    protected function nonAuthorized($response)
    {
        return $response->withStatus(401);
    }

    protected function notFound($response)
    {
        return $response->withStatus(404);
    }

    protected function badRequest($response)
    {
        return $response->withStatus(400);
    }

    protected function isAuthorized()
    {
        return isset($_SESSION['user']);
    }
}