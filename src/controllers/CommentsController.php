<?php
namespace app\controllers;

use app\models\active_records\Comment;

class CommentsController extends Controller
{
    private $renderer;

    public function __construct($renderer)
    {
        $this->renderer = $renderer;
    }

    public function addComment($request, $response)
    {
        if (!$this->isAuthorized()) {
            return $this->nonAuthorized($response);
        }

        $data = $request->getParsedBody();

        if (isset($data['postId']) && isset($data['text'])) {
            $comment = new Comment(
                NULL,
                NULL,
                $data['text'],
                $_SESSION['user']->id,
                $data['postId']);

            $comment->save();

            return $this->redirect($response, '/posts/' . $comment->postId);
        }

        return $this->badRequest($response);
    }
}