<?php

namespace app\controllers;

use app\models\active_records\Category;
use app\models\active_records\Post;
use app\models\active_records\User;

class UsersController extends Controller
{
    private $renderer;

    public function __construct($renderer)
    {
        $this->renderer = $renderer;
    }

    public function registerPage($response)
    {
        return $this->renderer->render($response, 'register.html', ['errors' => [], 'data' => ['name' => '', 'email' => '']]);
    }

    public function register($request, $response)
    {
        $data = $request->getParsedBody();

        $errors = [];

        if (isset($data['email']) && isset($data['name']) && isset($data['password'])) {
            if (User::isUniqueEmail($data['email'])) {
                if (User::isUniqueName($data['name'])) {
                    $user = new User();
                    $user->email = $data['email'];
                    $user->name = $data['name'];
                    $user->password = $data['password'];

                    $user->save();

                    $_SESSION['user'] = $user;

                    return $this->redirect($response, '/');
                } else {
                    $errors[] = 'Name is not unique';
                }
            } else {
                $errors[] = 'Email is not unique';
            }
        } else {
            $errors[] = 'Email, name and password are required';
        }

        return $this->renderer->render($response, 'register.html', ['errors' => $errors, 'data' => $data]);
    }

    public function loginPage($response)
    {
        return $this->renderer->render($response, 'login.html', ['errors' => []]);
    }

    public function login($request, $response)
    {
        $data = $request->getParsedBody();

        $errors = [];

        if (isset($data['email']) && isset($data['password'])) {
            $user = User::getByEmailAndPassword($data['email'], $data['password']);

            if ($user) {
                $_SESSION['user'] = $user;

                return $this->redirect($response, '/');
            }

            $errors[] = 'Invalid email or password';
        } else {
            $errors[] = 'Email and password are required';
        }

        return $this->renderer->render($response, 'login.html', ['errors' => $errors]);
    }

    public function logout($response)
    {
        $_SESSION['user'] = NULL;

        return $this->redirect($response, '/');
    }

    public function get($request, $response)
    {
        $id = $request->getAttributes()['id'];

        $user = User::getById($id);

        if (is_null($user)) {
            return $this->notFound($response);
        } else {
            $posts = Post::getUserPosts($id);

            $categories = Category::getAll();

            if (!isset($_SESSION['user'])) {
                $user = NULL;
            } else {
                $user = $_SESSION['user'];
            }

            return $this->renderer->render($response, 'user.html', ['userInfo' => $user, 'posts' => $posts, 'user' => $user, 'categories' => $categories]);
        }
    }
}