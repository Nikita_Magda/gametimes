<?php
namespace app\controllers;


use app\models\active_records\Category;
use app\models\active_records\Comment;
use app\models\active_records\Post;

class PostsController extends Controller
{
    private $renderer;

    public function __construct($renderer)
    {
        $this->renderer = $renderer;
    }

    public function createPage($request, $response)
    {
        $categories = Category::getAll();

        if (!isset($_SESSION['user'])) {
            $user = NULL;
        } else {
            $user = $_SESSION['user'];
        }

        return $this->renderer->render($response, 'create-post.html', ['categories' => $categories, 'user' => $user]);
    }

    public function create($request, $response)
    {
        if (!$this->isAuthorized()) {
            return $this->nonAuthorized($response);
        }

        $data = $request->getParsedBody();

        if (isset($data['title']) && isset($data['categoryId']) && isset($data['text'])) {
            $post = new Post(
                NULL,
                $data['title'],
                NULL,
                $data['text'],
                $_SESSION['user']->id,
                $data['categoryId']);

            $post->save();

            return $this->redirect($response, '/posts/' . $post->id);
        }

        $categories = Category::getAll();
        return $this->renderer->render($response, 'create-post.html', ['categories' => $categories]);
    }

    public function get($request, $response)
    {
        $postId = $request->getAttribute('id');

        $post = Post::getById($postId);

        if (is_null($post)) {
            return $this->notFound($response);
        }

        if (!isset($request->getQueryParams()['commentsPage'])) {
            $commentsPage = 1;
        } else {
            $commentsPage = $request->getQueryParams()['commentsPage'];
        }

        $take = 10;

        $skip = ($commentsPage - 1) * $take;

        $comments = Comment::getForPost($postId, $skip, $take);
        $categories = Category::getAll();

        if (!isset($_SESSION['user'])) {
            $user = NULL;
        } else {
            $user = $_SESSION['user'];
        }

        return $this->renderer->render(
            $response,
            'post.html',
            ['post' => $post, 'comments' => $comments, 'categories' => $categories, 'user' => $user]);
    }

    public function search($request, $response)
    {
        $params = $request->getQueryParams();

        $posts = [];
        $searchQuery = '';

        if (isset($params['query'])) {
            $searchQuery = $params['query'];
        } else {
            $searchQuery = '';
        }

        $categoryId = 0;

        if (!isset($params['categoryId']) || $params['categoryId'] == 0) {
            $posts = Post::search($searchQuery);
        } else {
            $categoryId = $params['categoryId'];
            $posts = Post::searchWithCategory($searchQuery, $params['categoryId']);
        }

        $categories = Category::getAll();

        if (!isset($_SESSION['user'])) {
            $user = NULL;
        } else {
            $user = $_SESSION['user'];
        }

        $this->renderer->render($response, 'search.html', ['query' => $searchQuery, 'categoryId' => $categoryId, 'posts' => $posts, 'categories' => $categories, 'user' => $user]);
    }
}