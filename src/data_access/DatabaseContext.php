<?php
namespace app\data_access;

class DatabaseContext
{
    private $database;

    public function __construct()
    {
        try {
            $this->database = new \PDO(
                'mysql:host=localhost;dbname=game_times_db',
                'root',
                '',
                array(
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_PERSISTENT => true,
                    \PDO::ATTR_EMULATE_PREPARES => false
                ));
        } catch(\PDOException $ex) {
            echo "Can't establish connection with database.";
            print_r($ex);
        }
    }

    public function executeQuery($query, array $parameters = array())
    {
        try {
            if ($this->database != null) {
                $statement = $this->database->prepare($query);

                $result = $statement->execute($parameters);

                if ($result) {
                    return $statement->fetchAll(\PDO::FETCH_ASSOC);
                } else {
                    return array();
                }
            }
        } catch(\PDOException $ex) {
            print_r($ex);
            die;
        }

        return NULL;
    }
}