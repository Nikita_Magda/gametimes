<?php
namespace app\models\active_records;

class User
{
    public $id;
    public $name;
    public $email;
    public $password;

    public function __construct($id = NULL, $name = NULL, $email = NULL, $password = NULL)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    public function save() 
    {
        if (is_null($this->id)) {
            // Insert new value
            ActiveRecord::getContext()->executeQuery(
                'INSERT INTO users (name, email, password) VALUES (:name, :email, :password)',
                [':name' => $this->name, ':email' => $this->email, ':password' => $this->password]);

            $this->id = User::getByName($this->name)->id;
        } else {
            // Update existing
            ActiveRecord::getContext()->executeQuery(
                'UPDATE users SET name = :name, email = :email, password = :password WHERE id = :id',
                [':id' => $this->id, ':name' => $this->name, ':email' => $this->email, ':password' => $this->password]);
        }
    }

    public static function getById($id)
    {
        $result = ActiveRecord::getById('users', $id);

        if (empty($result)) {
            return NULL;
        }

        $singleUser = $result[0];
        
        return new User(
            $singleUser['id'], 
            $singleUser['name'], 
            $singleUser['email'], 
            $singleUser['password']);
    }

    public static function getByName($name)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM users WHERE name = :name', 
            [':name' => $name]);

        if (empty($result)) {
            return NULL;
        }

        $singleUser = $result[0];
        
        return new User(
            $singleUser['id'], 
            $singleUser['name'], 
            $singleUser['email'], 
            $singleUser['password']);
    }

    public static function getByEmailAndPassword($email, $password)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM users WHERE email = :email AND password = :password', 
            [':email' => $email, ':password' => $password]);

        if (empty($result)) {
            return NULL;
        }
        
        $singleUser = $result[0];
        
        return new User(
            $singleUser['id'], 
            $singleUser['name'], 
            $singleUser['email'], 
            $singleUser['password']);
    }

    public static function isUniqueName($name)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM users WHERE name = :name',
            [':name' => $name]);

        return empty($result);
    }

    public static function isUniqueEmail($email)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM users WHERE email = :email',
            [':email' => $email]);

        return empty($result);
    }
}