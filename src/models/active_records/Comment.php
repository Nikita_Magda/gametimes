<?php
namespace app\models\active_records;

class Comment
{
    public $id;
    public $date;
    public $text;
    public $userId;
    public $user;
    public $postId;
    public $post;

    public function __construct($id, $date, $text, $userId, $postId) 
    {
        $this->id = $id;
        $this->date = $date;
        $this->text = $text;
        $this->userId = $userId;
        $this->postId = $postId;

        if (!is_null($this->userId))
        {
            $this->user = User::getById($this->userId);
        }

        if (!is_null($this->postId))
        {
            $this->post = Post::getById($this->postId);
        }
    }

    public function save() 
    {
        if (is_null($this->id)) {
            // Insert new value
            ActiveRecord::getContext()->executeQuery(
                'INSERT INTO comments (date, text, user_id, post_id) VALUES (NOW(), :text, :user_id, :post_id)',
                [':text' => $this->text, ':user_id' => $this->userId, ':post_id' => $this->postId]);
        } else {
            // Update existing
            ActiveRecord::getContext()->executeQuery(
                'UPDATE comments SET text = :text, user_id = :user_id, post_id = :post_id WHERE id = :id',
                [':id' => $this->id, ':text' => $this->text, ':user_id' => $this->userId, ':post_id' => $this->postId]);
        }
    }

    public static function getById($id)
    {
        $result = ActiveRecord::getById('comments', $id);

        if (empty($result)) {
            return NULL;
        }

        $result = $result[0];
        
        return new Comment(
            $result['id'],
            $result['date'], 
            $result['text'], 
            $result['user_id'], 
            $result['post_id']);
    }

    public static function getForPost($postId, $skip = 0, $take = 100)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM comments WHERE post_id = :post_id ORDER BY date DESC LIMIT :limit OFFSET :offset',
            [':post_id' => $postId, ':limit' => $take, ':offset' => $skip]
        );

        $comments = [];

        foreach ($result as $commentResult) {
            $comments[] = new Comment(
                $commentResult['id'],
                $commentResult['date'],
                $commentResult['text'],
                $commentResult['user_id'],
                $commentResult['post_id']);
        }

        return $comments;
    }

    public static function countAllForPost($postId)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT COUNT(*) AS count FROM comments WHERE post_id = :post_id',
            [':post_id' => $postId]
        );

        return $result['count'];
    }
}