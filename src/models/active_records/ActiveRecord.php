<?php
namespace app\models\active_records;

use app\data_access\DatabaseContext;

class ActiveRecord
{
    public static function getContext()
    {
        static $context;

        if (is_null($context)) {
            $context = new DatabaseContext();
        }

        return $context;
    }

    public static function getById($table, $id)
    {
        return self::getContext()->executeQuery(
            'SELECT * FROM ' . $table . ' WHERE id = :id', 
            [':id' => $id]);
    }
}