<?php
namespace app\models\active_records;

class Post
{
    public $id;
    public $title;
    public $date;
    public $text;
    public $userId;
    public $user;
    public $categoryId;
    public $category;

    public function __construct($id, $title, $date, $text, $userId, $categoryId) 
    {
        $this->id = $id;
        $this->title = $title;
        $this->date = $date;
        $this->text = $text;
        $this->userId = $userId;
        $this->categoryId = $categoryId;

        $this->preview = Post::getPreview($text);

        if (!is_null($this->userId))
        {
            $this->user = User::getById($this->userId);
        }

        if (!is_null($this->categoryId))
        {
            $this->category = Category::getById($this->categoryId);
        }
    }

    public function save() 
    {
        if (is_null($this->id)) {
            // Insert new value
            ActiveRecord::getContext()->executeQuery(
                'INSERT INTO posts (title, date, text, user_id, category_id) VALUES (:title, NOW(), :text, :user_id, :category_id)',
                [':title' => $this->title, ':text' => $this->text, ':user_id' => $this->userId, ':category_id' => $this->categoryId]);

            $this->id = ActiveRecord::getContext()->executeQuery('SELECT id FROM posts WHERE title = :title', [':title' => $this->title])[0]['id'];
        } else {
            // Update existing
            ActiveRecord::getContext()->executeQuery(
                'UPDATE users SET title = :title, text = :text, user_id = :user_id, category_id = :category_id WHERE id = :id',
                [':id' => $this->id, ':title' => $this->title, ':text' => $this->text, ':user_id' => $this->userId, ':category_id' => $this->categoryId]);
        }
    }

    public static function getPreview($text)
    {
        $paragraphEnd = strpos($text, '</p>');

        return substr($text, 0, $paragraphEnd + 5);
    }

    public static function getById($id)
    {
        $result = ActiveRecord::getById('posts', $id);

        if (empty($result)) {
            return NULL;
        }

        $result = $result[0];
        
        return new Post(
            $result['id'], 
            $result['title'], 
            $result['date'], 
            $result['text'], 
            $result['user_id'], 
            $result['category_id']);
    }

    public static function getNewest($skip = 0, $take = 100)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM posts ORDER BY date DESC LIMIT :limit OFFSET :offset',
            [':limit' => $take, ':offset' => $skip]
        );

        return Post::mapSelectResult($result);
    }

    public static function countAll() 
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT COUNT(*) AS count FROM posts',
            []
        );

        return $result['count'];
    }

    public static function search($searchQuery) 
    {
        $likePattern = '%' . $searchQuery . '%';

        $result = ActiveRecord::getContext()->executeQuery(
            '(SELECT * FROM posts WHERE UPPER(title) LIKE UPPER(:title_like_pattern) ORDER BY date DESC)'
            . ' UNION (SELECT * FROM posts WHERE UPPER(text) LIKE UPPER(:text_like_pattern) ORDER BY date DESC)',
            [':text_like_pattern' => $likePattern, ':title_like_pattern' => $likePattern]
        );

        return Post::mapSelectResult($result);
    }

    public static function searchWithCategory($searchQuery, $categoryId)
    {
        $likePattern = '%' . $searchQuery . '%';

        $result = ActiveRecord::getContext()->executeQuery(
            '(SELECT * FROM posts WHERE UPPER(title) LIKE UPPER(:title_like_pattern) AND category_id = :title_category_id ORDER BY date DESC)'
            . ' UNION (SELECT * FROM posts WHERE UPPER(text) LIKE UPPER(:text_like_pattern) AND category_id = :text_category_id ORDER BY date DESC)',
            [':text_like_pattern' => $likePattern, ':title_like_pattern' => $likePattern, ':title_category_id' => $categoryId, ':text_category_id' => $categoryId]
        );

        return Post::mapSelectResult($result);
    }

    public static function getUserPosts($userId) 
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM posts WHERE user_id = :user_id ORDER BY date DESC',
            [':user_id' => $userId]
        );

        return Post::mapSelectResult($result);
    }

    private static function mapSelectResult($result) 
    {
        $posts = [];

        foreach ($result as $postResult) {
            $posts[] = new Post(
                $postResult['id'],
                $postResult['title'],
                $postResult['date'],
                $postResult['text'],
                $postResult['user_id'],
                $postResult['category_id']);
        }

        return $posts;
    }
}