<?php
namespace app\models\active_records;

class Category
{
    public $id;
    public $name;
    public $postsCount;

    public function __construct($id, $name) 
    {
        $this->id = $id;
        $this->name = $name;

        if (!is_null($this->id)) {
            $this->postsCount = Category::getPostsCount($this->id);
        }
    }

    public static function getById($id)
    {
        $result = ActiveRecord::getById('categories', $id);

        if (empty($result)) {
            return NULL;
        }
        
        $result = $result[0];
        
        return new Category(
            $result['id'],
            $result['name']);
    }

    public static function getAll()
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT * FROM categories ORDER BY name', 
            []);

        $categories = [];

        foreach ($result as $categoryResult) {
            $categories[] = new Category($categoryResult['id'], $categoryResult['name']);
        }
        
        return $categories;
    }

    private static function getPostsCount($id)
    {
        $result = ActiveRecord::getContext()->executeQuery(
            'SELECT COUNT(*) AS count FROM posts WHERE category_id = :id',
            [':id' => $id]);

        return $result[0]['count'];
    }
}